package com.example.mobile.rogelfinalexamy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    private EditText etusername,etpassword ;
    private String username,password,fname,lname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        etusername = (EditText) findViewById(R.id.etUsername);
        etpassword = (EditText) findViewById(R.id.etPassword);
        final Button btnSignUp = (Button) findViewById(R.id.btnSignUp);
        final Button btnLogin = (Button) findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etusername.getText() != null && etusername.getText().toString().equals(username)&& etpassword.getText() != null && etpassword.getText().toString().equals(password)) {
                    Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
                    intent.putExtra(Data.ACCOUNT_USERNAME, username);
                    intent.putExtra(Data.ACCOUNT_PASSWORD, password);
                    intent.putExtra(Data.ACCOUNT_FNAME, fname);
                    intent.putExtra(Data.ACCOUNT_LNAME, lname);
                    startActivity(intent);
                } else {
                    Toast.makeText(LoginActivity.this, "Login failed", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });

    }
    @Override
    protected void onResume() {
        super.onResume();
        if (getIntent() == null) return;

        Bundle bundle = getIntent().getExtras();

        if (bundle == null) return;

        username = bundle.getString(Data.ACCOUNT_USERNAME);
        password = bundle.getString(Data.ACCOUNT_PASSWORD);
        fname = bundle.getString(Data.ACCOUNT_FNAME);
        lname = bundle.getString(Data.ACCOUNT_LNAME);
    }
}