package com.example.mobile.rogelfinalexamy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class AnimationActivity extends AppCompatActivity {
    int press=0;
    ImageView img1,img2;
    public void animateImage (View view )
    {
        img1 = (ImageView)findViewById(R.id.img1);
        img2 = (ImageView)findViewById(R.id.img2);
        if(press%2==0) {

            img1.animate()
                    //.translationXBy(1000f)
                    .rotationBy(3600)
                    .alpha(0f)
                    .setDuration(2000);
            img2.animate()
                    //.translationXBy(1000f)
                    .rotationBy(3600)
                    .alpha(1f)
                    .setDuration(2000);
            press++;
        }

        else {
            img1.animate()
                    //.translationXBy(1000f)
                    .rotationBy(3600)
                    .alpha(1f)
                    .setDuration(2000);
            img2.animate()
                    //.translationXBy(1000f)
                    .rotationBy(3600)
                    .alpha(0f)
                    .setDuration(2000);
            press++;
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation);

    }

}
