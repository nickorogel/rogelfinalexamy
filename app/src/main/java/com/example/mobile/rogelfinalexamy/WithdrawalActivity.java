package com.example.mobile.rogelfinalexamy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class WithdrawalActivity extends AppCompatActivity {
    private Button mWithdrawButton;
    private EditText mAmountEditText;
    private EditText mAccountPinWithdrawalEditText;
    private TextView mNewBalanceTextView;

    private float mAmountToWithdraw;
    private String mAccountPinInput;
    private String Pin,Account;
    private float balance;
    private TextView txtbalance;
    private Button CheckBalance;
    private EditText PinNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdrawal);
        Intent intent = getIntent();
        balance = intent.getExtras().getFloat(Data.ACCOUNT_BALANCE);
        Pin = intent.getExtras().getString(Data.ACCOUNT_PIN);
        mNewBalanceTextView = (TextView) findViewById(R.id.newBalanceTextView);

        mWithdrawButton = (Button) findViewById(R.id.withdrawButton);
        mAmountEditText = (EditText) findViewById(R.id.amountEditText);
        mAccountPinWithdrawalEditText = (EditText) findViewById(R.id.accountPinWithdrawalEditText);

        mWithdrawButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    mAccountPinInput = mAccountPinWithdrawalEditText.getText().toString();

                    Log.d("test", mAccountPinInput);

                    if (mAccountPinInput.length() != 0 && mAccountPinInput.equals(Pin)) {

                        mAmountToWithdraw = Float.valueOf(mAmountEditText.getText().toString());

                        Log.d("test", mAmountToWithdraw + "");

                        if (balance >= mAmountToWithdraw) {
                            balance -= mAmountToWithdraw;
                            Log.d("test", balance + "account balance");

                            mNewBalanceTextView.setText(String.format("%s: %f",
                                    "New Balance",balance));

                        } else if (balance < mAmountToWithdraw && balance >= 0) {
                            Toast.makeText(WithdrawalActivity.this, "Insufficient funds!", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(WithdrawalActivity.this, "Incorrect PIN!", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(WithdrawalActivity.this, "Incorrect PIN!", Toast.LENGTH_SHORT).show();
                    Log.d("test", mAmountToWithdraw + "crash");

                }

            }
        });
    }
}
