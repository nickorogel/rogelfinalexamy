package com.example.mobile.rogelfinalexamy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MenuActivity extends AppCompatActivity {
    private String username;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        final Button btntictactoe = (Button) findViewById(R.id.btntictactoe);
        final Button btnanimation = (Button) findViewById(R.id.btnAnimation);
        final TextView username1 = (TextView) findViewById(R.id.textView2);
        final Button btnATM = (Button) findViewById(R.id.btnATM);
        final Button btntriangular = (Button) findViewById(R.id.btntriangular);
        username=(getIntent().getExtras().getString(Data.ACCOUNT_USERNAME));
        username1.setText("Hello,"+username);
        btnanimation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, AnimationActivity.class);
                startActivity(intent);
            }
        });
        btnATM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, AtmActivity.class);
                startActivity(intent);
            }
        });
        btntriangular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, TriangularActivity.class);
                startActivity(intent);
            }
        });
        btntictactoe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, TictactoeActivity.class);
                startActivity(intent);
            }
        });
    }
}
