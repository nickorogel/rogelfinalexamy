package com.example.mobile.rogelfinalexamy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity {
    private EditText etusername,etpassword,etfname,etlname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        etfname = (EditText) findViewById(R.id.etFname);
        etlname = (EditText) findViewById(R.id.etLname);
        etusername= (EditText) findViewById(R.id.etUsername);
        etpassword= (EditText) findViewById(R.id.etPassword);

        final Button btnConfirm = (Button) findViewById(R.id.btnConfirm);

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerUser();
            }
        });
    }
    private void registerUser(){
       String fname = etfname.getText().toString();
        String lname = etlname.getText().toString();
        String username = etusername.getText().toString();
        String password = etpassword.getText().toString();


        if(TextUtils.isEmpty(fname)){
            Toast.makeText(this, "Please enter First Name", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(lname)){
            Toast.makeText(this, "Please enter Last Name", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(username)){
            Toast.makeText(this, "Please enter Username", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(password)){
            Toast.makeText(this, "Please enter Password", Toast.LENGTH_SHORT).show();
            return;
        }
        else{
            Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
            intent.putExtra(Data.ACCOUNT_USERNAME, username);
            intent.putExtra(Data.ACCOUNT_PASSWORD, password);
            intent.putExtra(Data.ACCOUNT_FNAME, fname);
            intent.putExtra(Data.ACCOUNT_LNAME, lname);

            startActivity(intent);
        }
    }}
