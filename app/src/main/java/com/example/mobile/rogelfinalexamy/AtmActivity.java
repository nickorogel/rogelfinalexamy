package com.example.mobile.rogelfinalexamy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AtmActivity extends AppCompatActivity {
    private String Pin;
    private float balance;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atm);
       final EditText PinNumber= (EditText) findViewById(R.id.etPinNumber);
        final Button CheckBalance = (Button) findViewById(R.id.btnCheckBalance);
        final Button withdrawal = (Button) findViewById(R.id.btnWithdraw);
        final EditText balance1 = (EditText) findViewById(R.id.etBalance);
        withdrawal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AtmActivity.this, WithdrawalActivity.class);
                Float balance = Float.valueOf(balance1.getText().toString());
                String Pin = PinNumber.getText().toString();
                intent.putExtra(Data.ACCOUNT_PIN, Pin);
                intent.putExtra(Data.ACCOUNT_BALANCE, balance);
                startActivity(intent);
            }
        });
        CheckBalance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AtmActivity.this, BalanceActivity.class);
                Float balance = Float.valueOf(balance1.getText().toString());
                String Pin = PinNumber.getText().toString();
                intent.putExtra(Data.ACCOUNT_PIN, Pin);
                intent.putExtra(Data.ACCOUNT_BALANCE, balance);
                startActivity(intent);
            }
        });
    }
}
