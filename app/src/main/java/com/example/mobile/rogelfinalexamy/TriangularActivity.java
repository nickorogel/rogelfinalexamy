package com.example.mobile.rogelfinalexamy;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class TriangularActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_triangular);

        final TextView txtresult = (TextView) findViewById(R.id.txtresult);
        final EditText etnumber = (EditText) findViewById(R.id.etnumber);
        final Button btexucte = (Button) findViewById(R.id.btexecute);
        final Button btnreveal = (Button) findViewById(R.id.btnreveal);


        btexucte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int result = 0;

                String str = etnumber.getText().toString();

                if (str.isEmpty()) {
                    return;
                }

                for (int x= 1; x<=Integer.parseInt(etnumber.getText().toString()); x++){
                    result=result + x;
                    txtresult.setText("\nThe Triangular of " + String.valueOf(x)+" is "+String.valueOf(result));

                }

            }




        });


        btnreveal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {;

                int result1= 0;

                for (int x= 1; x<=10; x++){
                    result1=result1 + x;
                    txtresult.append("\nThe Triangular of " + String.valueOf(x)+" is "+String.valueOf(result1));

                }
            }
        });


    }
}