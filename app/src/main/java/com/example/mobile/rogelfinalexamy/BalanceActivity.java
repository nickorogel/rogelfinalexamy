package com.example.mobile.rogelfinalexamy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class BalanceActivity extends AppCompatActivity {
    private String Pin;
    private float balance;
    private TextView txtbalance,PinNumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance);

        txtbalance = (TextView) findViewById(R.id.textbalanceno);
        PinNumber = (TextView) findViewById(R.id.textPinno);



        Intent intent = getIntent();
        balance = intent.getExtras().getFloat(Data.ACCOUNT_BALANCE);
        Pin = intent.getExtras().getString(Data.ACCOUNT_PIN);

        txtbalance.setText(balance + "");
        PinNumber.setText(Pin + "");

    }
}
