package com.example.mobile.rogelfinalexamy;

/**
 * Created by nicko on 9/16/2017.
 */

public class Data {
    static String ACCOUNT_USERNAME = "ACCOUNT_USERNAME";
    static String ACCOUNT_PASSWORD = "ACCOUNT_PASSWORD";
    static String ACCOUNT_FNAME = "ACCOUNT_FNAME";
    static String ACCOUNT_LNAME = "ACCOUNT_LNAME";
    static String ACCOUNT_NUMBER = "ACCOUNT_NUMBER";
    static String ACCOUNT_BALANCE = "ACCOUNT_BALANCE";
    static String ACCOUNT_PIN = "ACCOUNT_PIN";
}
